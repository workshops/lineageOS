# Steps to flash microG LineageOS on Samsung Galaxy S5 Plus

## Prerequisites

- A Samsung Galaxy S5 Plus (make sure it says SM-G901F in the "About Device" settings)
- A computer (preferably Linux) with:
  - an USB slot
  - [adb](https://wiki.ubuntuusers.de/adb/) installed
  - [heimdall-flash](https://wiki.ubuntuusers.de/Heimdall/) installed (on
    Windows you can use [Odin](https://www.odinflash.com/))
- A micro-USB cable which can transfer data

## Preparations

download latest TWRP image from https://dl.twrp.me/kccat6/

download latest lineageOS from https://download.lineage.microg.org/kccat6/

download latest su (arm) for 16.0 from https://download.lineageos.org/extras

## Unlock the Bootloader / OEM

Attach phone to PC. Use a good micro USB cable; make sure that you can use it
to copy data.

enable developer options

enable USB debugging in developer options

allow USB debugging access for your PC

## Flash Recovery

*Right now, flashing recovery only works with Odin; Odin only runs on Windows.
Guide how to flash TWRP with Odin:
https://www.cyanogenmods.org/forums/topic/install-twrp-recovery-samsung-android-using-odin/

Otherwise, there is also this guide to build a patched version of heimdall,
which works with the S5 Plus:
https://github.com/Benjamin-Dobell/Heimdall/pull/225#issuecomment-180961953
Actually, Heimdall might work with some S5s, just not with mine.

If Heimdall works:*

Power off the your device and connect the USB adapter to the computer (but not to the device, yet).

Boot into download mode:

    With the device powered off, hold Volume Down + Home + Power.

Accept the disclaimer, then insert the USB cable into the device.

`heimdall flash --RECOVERY twrp-3.*-kccat6.img`

Now we don't want to boot into the system, we want into the new recovery
directly - so turn off the phone with the power button, and then boot into
recovery by holding the power button, the home button, and volume up for a few
seconds. Release them only when you see the blue "Recovery" writing in the top
left of the screen.

When asked, swipe right to allow modifications.

Then go to Wipe + Advanced Wipe to wipe the following partitions:

* System
* Data
* Cache
* Dalvik

Then go to Advanced/ADB Sideload, swipe to the right, and execute `adb sideload lineage-xxx.zip` on your computer.
* When it shows something like `Total xfer: 1.00x` or `adb: failed to read command: Success` on the PC, and `Exit code: 1.000000` in TWRP, it has suceeded.

Don't reboot yet, we still want to flash addonsu to root the phone:
Go to Advanced/ADB Sideload, swipe to the right, and execute `adb sideload addonsu-16.0-arm-signed.zip` on your computer.
* When it shows something like `Total xfer: 1.00x` or `adb: failed to read command: Success` on the PC, and `Exit code: 1.000000` in TWRP, it has suceeded.

Reboot to start LineageOS!

### Finish Installation

Go through the setup screen and choose what you want. You can be as restrictive
as you want. Make sure to choose a Wi-Fi, so you can proceed with the next
steps; you can also do this at any later time. 

You should also choose a screen lock pin. Patterns are not very secure (you can
often see them by holding the phone into the light at the right angle).
Passwords are a bit inconvenient (but most secure). Do *not* use a fingerprint
sensor, they are really insecure.

## Device Encryption

Charge the phone to 100%.

Go to the Security Settings and encrypt the phone. During the process, it
reboots once and asks you for the device PIN. It can take a while, but not soo
long actually.

### Secure the device encryption

You usually want to have a long password for your device encryption, which you
only have to enter when the phone boots, and a shorter PIN for your lockscreen.
By design, Android doesn't allow this; this is a workaround to make it possible
still.

We will also install an app which shuts down the phone when you get the unlock
code wrong 5 times in a row.

#### Wrong PIN Shutdown

Open the F-Droid app, wait until it is done with "Updating repositories".

Install the app "Wrong PIN Shutdown".

Now go to the developer settings, and enable root for Apps and ADB.

Open "Wrong PIN Shutdown", grant Admin rights until forever, activate the
function, and set it to shut down the phone after 5 wrong tries to enter the
PIN. You can try it out directly - if you enter your PIN wrong 5 times, does it
shut down? 

#### Change the encryption password to a long password

First you need to enable USB debugging again. You can enable the developer
options at "About Phone", tap the "build number" seven times. After you enabled
it, you can find the Developer options in the settings at "System > Advanced >
Developer Options". Enable USB debugging and reboot into the bootloader mode:

Then go to the Security settings and set the PIN you want to use for your
lockscreen.

Now you have to backup the files where it is saved. To do so, execute these
commands with adb - you need to be connected to a PC with a proper USB cable:

```
adb root
adb pull /data/system_de/0/spblob/
adb pull /data/misc/keystore/user_0/1000_USRPKEY_synthetic_password_*  # the file has a different name; just remove the * and press tab before executing it
adb pull /data/system/locksettings.db
```

Now, go to the Security settings again, and change your PIN to the password you
want to use for the encryption. Unfortunately you can use maximally 17
characters.

Finally, you use adb again to restore the backup of the PIN, which changes your
lockscreen PIN, but not your encryption passphrase:

```
adb push locksettings.db /data/system/locksettings.db
adb push 1000_USRPKEY_synthetic_password_* /data/misc/keystore/user_0/  # the filename will be different with your device
adb push spblob/ /data/system_de/0/
```

When you try to unlock your screen now, you will realize that you still have to
enter the encryption passphrase. So reboot the phone; enter the encryption
passphrase at startup, as desired. When the phone has booted, you can use the
(shorter) PIN to unlock your phone.

