# Steps to flash microG LineageOS on Sony Xperia SP

## Prerequisites

- A Sony Xperia SP (make sure it says `huashan` in the "About Device" settings)
- A computer (preferably Linux) with:
  - an USB slot
  - [adb](https://wiki.ubuntuusers.de/adb/) installed
  - [heimdall-flash](https://wiki.ubuntuusers.de/Heimdall/) installed (on
    Windows you can use [Odin](https://www.odinflash.com/))
- A micro-USB cable which can transfer data

## Preparations

download latest TWRP image from https://dl.twrp.me/huashan/

download latest lineageOS from https://download.lineage.microg.org/huashan/

rename TWRP image to recovery.img

## Unlock the Bootloader / OEM

enable developer options

enable USB debugging in developer options

allow USB debugging access for your PC

unlock bootloader: https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/how-to-unlock-bootloader/

## Flash TWRP

`adb reboot bootloader`

wait until bootloader mode is active: blue LED 

fastboot flash boot recovery.img

fastboot reboot

## Flash LineageOS

First boot into the recovery mode:

* Either with adb: adb reboot recovery
* Or with the recovery mode key combination of your device.
  * Note: this is not the same combination for the download mode.

Then go to Wipe + Advanced Wipe to wipe the following partitions:

* System
* Data
* Cache
* Dalvik

Then go to Advanced/ADB Sideload, swipe to the right, and execute `adb sideload lineage-xxx.zip` on your computer.
* When it shows `adb: failed to read command: Success` on the PC, and `Exit code: 1.000000` in TWRP, it has suceeded.

Reboot to start LineageOS!

