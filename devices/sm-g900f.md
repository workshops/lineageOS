# Steps to flash microG LineageOS on Samsung Galaxy S5

## Prerequisites

- A Samsung Galaxy S5 (make sure it says SM-G900F in the "About Device" settings)
- A computer (preferably Linux) with:
  - an USB slot
  - [adb](https://wiki.ubuntuusers.de/adb/) installed
  - [heimdall-flash](https://wiki.ubuntuusers.de/Heimdall/) installed (on
    Windows you can use [Odin](https://www.odinflash.com/))
- A micro-USB cable which can transfer data

## Preparations

download latest TWRP image from https://eu.dl.twrp.me/klte/

download latest lineageOS from https://cloud.0x90.space/s/5gIjkxmfvoHAAJL

download latest Magisk.zip from https://github.com/topjohnwu/Magisk/releases

## Unlock the Bootloader / OEM

Attach phone to PC. Use a good micro USB cable; make sure that it can be used
to copy data.

In the "About Device" settings,
enable developer options
by tapping on the "Build Number" 7 times.

Under "Developer options",
enable USB debugging.

Then when it asks you,
allow USB debugging access for your PC.

## Flash Recovery

Power off the your device and connect the USB adapter to the computer (but not to the device, yet).

Boot into download mode:

    With the device powered off, hold Volume Down + Home + Power.

Accept the disclaimer, then insert the USB cable into the device.

Open a command line,
and use the `cd` command to navigate to the download folder,
or whereever you saved the TWRP and lineageOS files,
e.g. with `cd Downloads`.
Run `heimdall flash --RECOVERY twrp-3.7.0_9-0-klte.img --no-reboot` on the command line,
or replace the twrp-xxxxx.img part with the file name of the downloaded TWRP image.

Now we don't want to boot into the system, we want into the new recovery
directly - so turn off the phone with the power button, and then boot into
recovery by holding the power button, the home button, and volume up for a few
seconds. Release them only when you see the blue "Recovery" writing in the top
left of the screen.

## Flash LineageOS in TWRP

When asked, swipe right to allow modifications.

Then go to "Wipe > Advanced Wipe" to wipe the following partitions:

* System
* Data
* Cache
* Dalvik

Now go back to the "Wipe" menu,
click on "Format Data",
and enter "yes" when asked for confirmation.

Then go back to the TWRP main menu
and to "Advanced > ADB Sideload",
swipe to the right, and execute `adb sideload lineage-18.1-20230615-microG-klte.zip` on your computer.
* When it shows something like `Total xfer: 1.00x` or `adb: failed to read command: Success` on the PC, and `Exit code: 1.000000` in TWRP, it has suceeded.

<!--
On lineageOS 17.1, addonsu doesn't work anymore. That's why we have to flash Magisk instead.

Don't reboot yet, we still want to flash addonsu to root the phone:
Go to Advanced/ADB Sideload, swipe to the right, and execute `adb sideload addonsu-16.0-arm-signed.zip` on your computer.
* When it shows something like `Total xfer: 1.00x` or `adb: failed to read command: Success` on the PC, and `Exit code: 1.000000` in TWRP, it has suceeded.
-->

<!--
Don't reboot yet, we still want to flash Magisk to root the phone.

First go to Backup and select only the Boot partition. Under "Select Storage",
select "Internal Storage". Swipe to backup. Now copy the backup to your PC with
`adb pull /sdcard/TWRP`.

Then go to Advanced/ADB Sideload, check "Wipe Cache" and "Wipe Dalvik", swipe
to the right, and execute `adb sideload Magisk-*.zip` on your computer.
* When it shows something like `Total xfer: 1.60x` or `adb: failed to read
  command: Success` on the PC, and `- Flashing new boot image` in combination
  with `- Done` in TWRP, it has suceeded.
--> 
Reboot to start LineageOS!

### Finish Installation

Go through the setup screen and choose what you want. You can be as restrictive
as you want. Make sure to choose a Wi-Fi, so you can proceed with the next
steps; you can also do this at any later time. 

You should also choose a screen lock pin. Patterns are not very secure (you can
often see them by holding the phone into the light at the right angle).
Passwords are a bit inconvenient (but most secure). Do *not* use a fingerprint
sensor, they are really insecure.

Tip: if you use LineageOS 17.1 or higher, you should disable the storage
manager in the settings - it deletes videos & photos automatically after 90
days. This is a very weird default setting.

## Device Encryption

Charge the phone to 80%.
If that takes too long for you,
you can use the command line
to tell the phone that the battery is on 80%.
For this you need to
enable USB debugging again,
with the same "tap build number 7 times" trick as before.
After that, you can find the LineageOS developer options
under "System > Advanced > Developer Options".
Now you can run `adb shell dumpsys battery set level 80` on your PC.

Go to the Security Settings and encrypt the phone. During the process, it
reboots once and asks you for the device PIN. It can take a while, but not
longer than 10 minutes typically.

If it fails for some reason,
you can boot into TWRP,
"Wipe > Format Data" to reset the broken encryption,
and setup lineageOS again.

### Secure the device encryption

You usually want to have a long password for your device encryption, which you
only have to enter when the phone boots, and a shorter PIN for your lockscreen.
By design, Android doesn't allow this; this is a workaround to make it possible
still.

<!--
We will also install an app which shuts down the phone when you get the unlock
code wrong 5 times in a row.

#### Wrong PIN Shutdown

Open the F-Droid app, wait until it is done with "Updating repositories".

Install the app "Wrong PIN Shutdown".

Now go to the developer settings, and enable root for Apps and ADB.

Open "Wrong PIN Shutdown", grant Admin rights until forever, activate the
function, and set it to shut down the phone after 5 wrong tries to enter the
PIN. You can try it out directly - if you enter your PIN wrong 5 times, does it
shut down? 
-->
#### Change the encryption password to a long password

First you need to enable USB debugging again. You can enable the developer
options at "About Phone", tap the "build number" seven times. After you enabled
it, you can find the Developer options in the settings at "System > Advanced >
Developer Options". Enable USB debugging and reboot into the bootloader mode:

Then go to the Security settings and set the PIN you want to use for your
lockscreen.

Now you have to backup the files where it is saved. To do so, execute these
commands with adb - you need to be connected to a PC with a proper USB cable:

```
adb root
adb pull /data/system_de/0/spblob/
adb pull /data/misc/keystore/user_0/1000_USRPKEY_synthetic_password_*  # the file has a different name; just remove the * and press tab before executing it
adb pull /data/system/locksettings.db
```

Now, go to the Security settings again, and change your PIN to the password you
want to use for the encryption. Unfortunately you can use maximally 17
characters.
After that, reboot your phone.
You should need the long passphrase now,
for both the decryption
and unlocking the screen.

After rebooting, use adb again to restore the backup of the PIN, which changes your
lockscreen PIN, but not your encryption passphrase:

```
adb push locksettings.db /data/system/locksettings.db
adb push 1000_USRPKEY_synthetic_password_* /data/misc/keystore/user_0/  # the filename will be different with your device
adb push spblob/ /data/system_de/0/
```

When you try to unlock your screen now, you will realize that you still have to
enter the encryption passphrase. So reboot the phone; enter the encryption
passphrase at startup, as desired. When the phone has booted, you can use the
(shorter) PIN to unlock your phone.

