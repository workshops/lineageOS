# Erstes Mal

Wir hatten ein Galaxy S4, damit hat das flashen im Workshop nicht geklappt.
Der Besitzer hat es durch die Anleitung aber später mit reinlesen noch alleine
hingekriegt.  War aber frustrierend für die anderen Workshop-Teilnehmer.

Dauer 2 Stunden, content war recht improvisiert, aber ganz gut.

# Zweites Mal

Der Workshop hat jetzt so knapp 3 Stunden gedauert mit zwei kurzen Pausen.
Wir haben zeitlich nicht mehr geschafft:
* welche sinnvollen Apps noch installiert werden könnten
* wie man es mit Magisk rootet

Leider gab es zwischendurch beim Showcase einen längeren Hänger, weil wir es
nicht geschafft haben, TWRP zu flashen.  In der Pause sind dann viele gegangen.
Kurz darauf haben wir es aber noch hingekriegt.

Insgesamt zu lang, zu spät angefangen, zu wenig Zeit genommen. Schwer zu sagen,
was man am besten wegkürzen kann.

Die Athmosphäre war aber sehr schön. Viele haben sich bedankt, etc.

Es kam die Frage auf, ob man es auch mit Mac OS flashen kann; ja.

Außerdem:

- Fragen am Ende des Abschnitts besprechen?
- Praxisberatung mit individuellen Fragen nach dem Workshop?
- Sinnabschnitte klarer gliedern, evtl. nummerieren?
- Im Praxisteil im Handout graphisch markieren, was allgemein gültige Schritte
  sind und welche Punkte individuell erarbeitet werden müssen
- Fachbegriffe plastisch erklären, ansonsten außen vor lassen oder einfachere
  Varianten verwenden


