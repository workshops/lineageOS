# Google-Freie Smartphones

Die Quellen für unsere Behauptungen.

## Warum Google boykottieren?

Arten, wie Google einen trackt:
- Location Data:
  https://qz.com/1183559/if-youre-using-an-android-phone-google-may-be-tracking-every-move-you-make/
  - Früher sogar wenn Location ausgeschaltet war:
    https://www.theverge.com/2017/11/21/16684818/google-location-tracking-cell-tower-data-android-os-firebase-privacy
- Nutzungsverhalten in Apps:
  https://support.google.com/analytics/answer/2568878?hl=en
- Email-Metadaten & Inhalt:
  https://www.usnews.com/opinion/articles/2013/05/10/15-ways-google-monitors-you
- they don't track Call History & SMS:
  https://www.quora.com/Do-calls-logs-and-messages-automatically-sync-with-my-Google-account-on-my-Android-phone
  - But Facebook does, if you use the FB Messenger:
    https://www.theverge.com/2018/3/25/17160944/facebook-call-history-sms-data-collection-android
- Suchanfragen:
  https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy
- Browsing History:
  https://www.usnews.com/opinion/articles/2013/05/10/15-ways-google-monitors-you
- Kontakte: 
- Kalender: 
- Tinder trackt einen anders:
  https://www.theguardian.com/technology/2017/sep/26/tinder-personal-data-dating-app-messages-hacked-sold
- Google schneidet Gespräche mit und lässt auch menschen reinhören:
  https://www.inc.com/jason-aten/google-is-absolutely-listening-to-your-conversations-it-just-confirms-why-people-dont-trust-big-tech.html

Was macht Google damit?
- Wertet Daten automatisiert aus - das ist so allgemein anerkannt, man findet
  nur Berichte darüber, dass sie sogar versuchen, Statistiker
  wegzuautomatisieren:
  https://www.technologyreview.com/s/535041/automating-the-data-scientists/
- Benutzt diese Daten zu Werbezwecken:
  https://www.androidcentral.com/does-google-sell-your-data
- Arbeitet zumindest mit amerikanischen Behörden und 5-Eyes-Geheimdiensten zsm:
  https://www.androidcentral.com/does-google-sell-your-data

## Ist mein Gerät kompatibel mit Lineage und TWRP?

- Lineage- und TWRP-kompatibles Smartphone:
  - Lineage: https://download.lineage.microg.org/
  - TWRP: https://twrp.me/Devices/

## Step by Step

- https://wiki.links-tech.org/IT/Workshops/Google-Free-Handy
- https://shadow53.com/android/no-gapps/setup-guide/
- https://www.systemli.org/de/2018/04/29/google-freies-android.html


