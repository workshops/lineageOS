# Google-Freie Smartphones

Wer hat schon gerne ständig eine Wanze in der Hosentasche? iPhones sind nicht
die einzige Alternative zu Google Android. Mit dem Open Source-Betriebssystem
LineageOS und dem freien Appstore F-Droid, in dem alles kostenlos ist, kann man
sich ein gutes Smartphone gönnen, ohne sein Bewegungsprofil, Kontakte, und
andere Daten an Google zu verschenken.

Wir zeigen, 
- was damit möglich ist, 
- worauf man achten muss, 
- wie man sich so ein Handy selbst installiert, 
- oder wen man fragen kann, wenn man es sich nicht selbst zutraut.

Dauer ca. 3,5h inkl. drei Pausen.

Fragen bitte zwischen den Abschnitten, evtl werden sie gleich beantwortet.
Wenn ihr Fragen habt, wie konkret euer Gerät geflasht werden kann, bitte am
Ende.

Wenn wir zu schnell reden, oder ihr Begriffe nicht versteht - meldet euch. Es
ist wichtiger, dass ihr was lernt, als dass wir uns hier selbstdarstellen
können.

## Warum Google boykottieren?

Googles Geschäftsmodell ist Überwachungskapitalismus; sie versuchen so viel wie
möglich über einen rauszufinden, damit sie möglichst gute Werbung schalten
können.

Google hat TOS, die es ihnen erlauben, einen zu tracken, sobald man ihre
Services benutzt. 
- Auch wenn man nicht einmal weiß, dass man sie benutzt

Arten, wie Google einen trackt:
- Location Data
  - Alle möglichen Apps verlangen das: G Photos, G Maps, G Assistant, G App...
  - Opt-in: Es einer erlauben erlaubt es allen. Zeigt nicht an, wofür Daten
    noch verwendet werden.
  - Opt-out: möglich, aber wer weiß ob sie sich dran halten. Haben sie in der
    Vergangenheit auch mal nicht.
- Nutzungsverhalten in Apps
  - App-Entwickler binden Google-Analytics ein, um ihre Apps zu optimieren
  - Google speichert diese Daten und bereitet sie für Entwickler auf
  - Nichts hindert sie daran, die Daten selbst zu nutzen und weiterzuverkaufen
- Mit wem man kommuniziert und wann, wo, wie oft, etc.
  - E-Mail: Wenn man GMail nutzt oder mit jmd mit GMail kommuniziert.
  - Angeblich keine Call/SMS History.
  - Facebook speichert die aber, wenn man den Facebook Messenger benutzt.
- Was man kommuniziert
  - E-Mail: Wenn man GMail nutzt oder mit jmd mit GMail kommuniziert.
- Browsing History + Suchanfragen
  - Google-Suchanfragen werden auf ewig gespeichert.
  - Browser History: wenn man in Chrome in einen Google-Account eingeloggt ist,
    wird das synchronisiert.
- Kontakte-Sync: trackt damit auch all eure Freunde
  - kann man disablen, aber löschen tun sies wahrscheinlich nicht. 
  - Wenn ihr nicht wollt, dass Google euren Freundeskreis/Gruppe mappt, sollte
    niemand das benutzen.
- Kalender-Sync: und was ihr wann gemacht habt.
  - kann man disablen
- Virtual Assistant: Gespräche mitschneiden und zu Werbezwecken auswerten?
  - Google hat lange gesagt, sie schneiden erst ab "Ok Google" mit und werten
    es nicht aus
  - Naja; sie heuern extra Leute an, die die mitgeschnittenen Gespräche
    mithören, und aussortieren. Ist also bei weitem nicht perfekt.
  - Die lügen halt die ganze Zeit... da ist es jetzt auch nicht so
    unwahrscheinlich, dass sie das zu Werbezwecken auswerten.

Was macht Google damit?
- Google sammelt alle Daten die sie kriegen können
	- auch wenn sie noch nichts damit anfangen können
- Wertet sie automatisiert aus
- Trainiert damit Machine-Learning-Algorithmen
- Benutzt diese Daten zu Werbezwecken
- Arbeitet zumindest mit amerikanischen Behörden und 5-Eyes-Geheimdiensten
  zusammen

## Was ist LineageOS?

Lineage 
- freie Version von Android
- Google-Komponenten entfernt
	- Google Play Store
	- Google Location Services
	- Google Cloud (Kontakte & Kalender)
	- Google Cloud Messaging: Push Notifications
	- Google Maps
- microG: Open-Source replacements for GApps
	- network localization services
	- Google Cloud Messaging
	- Maps API

### Womit kann man Google Services ersetzen?

Dadurch fallen natürlich ein paar Sachen weg, man kann aber fast alle ersetzen.
- Suchanfragen: DuckDuckGo, Startpage, Ecosia
- Appstore: F-Droid oder YALP
- Location: Mozilla Location Provider
- Kontakte & Kalender Sync: DAVx5, zB über ownCloud
- Cloud: ownCloud
- Push Notifications: je nach App unterschiedlich, oder microG
- Maps: openstreetmap, zB mit OsmAnd~
- Corona-Tracking: mit Bluetooth gibt es leider nur mit Google; ist aber nicht
  allzu effektiv. Es gibt aber Open Source apps für Gastronom:innen.
- Andere Alternativen zu Google-Services:
  https://restoreprivacy.com/google-alternatives/

## Was braucht man für ein Google-freies Smartphone?

Man braucht:
- Rechner (am besten Linux, aber macOS oder Windows gehen auch)
- Android-Ladekabel
- evtl. microSD-Karte
- Lineage- und TWRP-kompatibles Smartphone, zB Samsung Galaxy S3:
  https://download.lineage.microg.org/

## Wie flasht man LineageOS auf ein Samsung Galaxy S3?

Siehe Step by Step-Anleitung; `step-by-step.md`

