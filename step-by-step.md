# Setup a Google-Free mobile phone 

Sources:

* https://shadow53.com/android/no-gapps/setup-guide/
* https://www.systemli.org/de/2018/04/29/google-freies-android.html

## Research 

* find out device code:
  https://support.mogaanywhere.com/hc/en-us/articles/201302920-How-do-I-find-my-Android-device-s-model-number-
* find out if there is TWRP for the device code: https://twrp.me/Devices/
* find out if there is a microG-lineage for the device code:
  https://download.lineage.microg.org/
  * if not possible, look for normal lineageOS without microG:
    https://wiki.lineageos.org/devices/

## Risks 

Liberating your device from google voids the warranty.

If it fails and your backup does not work correctly, you risk that contacts or
data are not available anymore.

If you have to use unofficial LineageOS or TWRP builds, because the device is
not supported, the installation can fail. Then you'll have to flash the stock
rom, and restore the backup. It's a nasty effort.

If you have to flash the stock rom, you can only login and use the phone, when
you log into a Google account, that already was logged into the phone before
you flashed it. Be wary if you bought the phone second hand/online.

## Step By Step

### Back up data before flashing

* Install F-Droid from their website: https://f-droid.org/
  * download apk
  * enable installing from "unknown sources" in Settings/Security/Unknown Sources
  * install apk
* install DAVDroid
* create ownCloud account
* backup telegram accounts
* backup files to ownCloud
* sync Contacts to ownCloud
* full backup: https://www.maketecheasier.com/back-up-android-data-adb-ubuntu/

### Install Firmware

Install TWRP and lineageOS+microG https://lineage.microg.org/#instructions

#### TWRP

Download TWRP image (see above)

Install ADB on the computer: https://www.xda-developers.com/install-adb-windows-macos-linux/

Enable USB debugging
* Tap seven times on Settings/About Phone/Build Number to enable Developer Settings
* Enable Settings/Developer Settings/USB Debugging

Create adb connection
* Connect phone to computer with a micro USB cable
* Linux: execute `adb devices -l`
* On the phone, confirm that you want to allow this computer to do debugging
* If the device doesn't show up, try a different micro USB cable

Boot the phone into download mode:
* Either `adb reboot bootloader`
* Or `adb reboot download`
* Or duckduckgo for the download mode key combination of your device
* If all of this doesn't work, you might need to unlock the bootloader:
  https://www.theandroidsoul.com/unlock-bootloader-via-fastboot-android/

Flash TWRP:
* Either with fastboot: `fastboot flash recovery twrp-xxx-xxx-xxx.img`
  * can be different, e.g. on the huashan device, the recovery partition is
    called boot. 
* Or with heimdall (you might need to install it first): `heimdall flash
  --RECOVERY twrp-xxx-xxx-xxx.img`
  * If it doesn't work, try `heimdall flash --RECOVERY twrp-xxx-xxx-xxx.img
    --no-reboot` and reboot manually afterwards. Careful: only reboot if the
    flashing is finished.

#### LineageOS

First boot into the recovery mode:
* Either with adb: `adb reboot recovery`
* Or with the recovery mode key combination of your device.
  * Note: this is not the same combination for the download mode.

Then go to Wipe + Advanced Wipe to wipe the following partitions:
* System
* Data
* Cache
* Dalvik

Then go to Advanced/ADB Sideload, swipe to the right, and execute `adb sideload
lineage-xxx.zip` on your computer.

If `adb sideload` doesn't work, use a micro SD card: 
* Copy the lineage-xxx.zip to the micro SD card
* Insert the micro SD card into the phone
* Boot into Recovery mode/TWRP
* Go to "Install", select the file from the micro SD card, and install it.

Reboot to start LineageOS!

### Secure Phone

* Encrypt phone
  * Load the battery to 100%
  * plug it into a charger
  * Start Encryption process: Settings/Security/Encrypt Phone
* Install F-Droid
* Root phone with Magisk
  * Download Magisk
    https://github.com/topjohnwu/Magisk/releases/download/v19.3/Magisk-v19.3.zip
  * Boot into TWRP recovery mode
  * Install Magisk zip file
  * Reboot
* Install cryptfs
  * First configure your lock screen PIN, then configure device encryption
    password
* Install wrong pin shutdown

### Install apps

On F-Droid, you can get open source apps for almost any purpose.
If you need Google Play Store apps, you can install them via the Aurora store.
You can install the Aurora Store on F-Droid, too.

Some recommendations: 

* DAVdroid: sync Contacts & Calendar with ownCloud
* Firefox Klar: private browser
* OsmAnd~: offline maps with https://openstreetmap.org
* Offi Stations: public transport connections. Different networks are available
* Briar: End-to-End encrypted serverless Messenger
* Conversations: End-to-End encrypted XMPP Messenger
* DeltaChat: End-to-End encrypted E-Mail Messenger
* ownCloud: store your data in a private cloud. Ask [[mailto:tech@lists.links-tech.org|tech@lists.links-tech.org]] for an account

And if you really want proprietary shit software on your device:

* setup Whatsapp with island https://forum.xda-developers.com/android/apps-games/closed-beta-test-incoming-companion-app-t3366295
* install FB messenger with island
* install Netflix or Amazon Video with YALP Store or Aurora https://www.reddit.com/r/LineageOS/comments/7c4b3i/netflix_and_losextrasaddonsu/

